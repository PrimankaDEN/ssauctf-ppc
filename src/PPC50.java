import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.Random;

public class PPC50 extends Thread {
    private static final int PORT = 6661;
    private static final String FLAG = "flag{U_R_STUPID_BOY}";

    private long num;
    private Socket socket;

    public static void main(String[] ar) {
        ServerSocket ss = null;
        try {
            ss = new ServerSocket(PORT);
            long i = 0;

            System.out.println("Waiting for a client...");

            while (true) {
                new PPC50(i, ss.accept());
                i++;
            }
        } catch (Exception e) {
            System.out.println("Init error: " + e);
        } finally {
            try {
                ss.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public PPC50(long num, Socket s) {
        this.num = num;
        this.socket = s;

        setDaemon(true);
        setPriority(NORM_PRIORITY);
        start();
    }

    @Override
    public void run() {
        try {
            long startTime = new Date().getTime();
            int neededAngel = new Random().nextInt(360);

            log("connected");
            InputStream sin = socket.getInputStream();
            OutputStream sout = socket.getOutputStream();

            DataOutputStream out = new DataOutputStream(sout);
            Writer w = new OutputStreamWriter(out, "UTF8");
            String rules = "Use decode(\"utf8\") if you see something strange\n\n" +
                    "\u0421\u043c\u043e\u0442\u0440\u0438\u0442\u0435\u043b\u044c: \"\u042d\u0442\u043e \u0442\u0432\u043e\u0439 \u043f\u0435\u0440\u0432\u044b\u0439 \u0440\u0430\u0431\u043e\u0447\u0438\u0439 \u0434\u0435\u043d\u044c, \u0430 \u0443 \u043d\u0430\u0441 \u0427\u041f!\n" +
                    "\u041c\u0438\u0441\u0442\u0435\u0440 \u0410\u0431\u0435\u0440\u043d\u0430\u0442\u0438 \u043e\u043f\u044f\u0442\u044c \u0437\u0430\u043f\u0435\u0440\u0441\u044f \u0443 \u0441\u0435\u0431\u044f \u0432 \u043a\u043e\u043c\u043d\u0430\u0442\u0435 \u0438 \u043d\u0438 \u0437\u0430 \u0447\u0442\u043e \u043d\u0435 \u0445\u043e\u0447\u0435\u0442 \u0432\u044b\u0445\u043e\u0434\u0438\u0442\u044c \u043e\u0442\u0442\u0443\u0434\u0430.\n" +
                    "\u0412\u043e\u0437\u044c\u043c\u0438 \u044d\u0442\u0443 \u0448\u043f\u0438\u043b\u044c\u043a\u0443 \u0434\u043b\u044f \u0432\u043e\u043b\u043e\u0441 \u0438 \u043e\u0442\u043a\u0440\u043e\u0439 \u044d\u0442\u0443 \u0447\u0435\u0440\u0442\u043e\u0432\u0443 \u0434\u0432\u0435\u0440\u044c, \u0441\u043f\u0440\u0430\u0432\u0438\u0448\u044c\u0441\u044f?\"\n" +
                    "\u0422\u044b \u0432\u0441\u0442\u0430\u0432\u043b\u044f\u0435\u0448\u044c \u0448\u043f\u0438\u043b\u044c\u043a\u0443 \u0432 \u0437\u0430\u043c\u043e\u0447\u043d\u0443\u044e \u0441\u043a\u0432\u0430\u0436\u0438\u043d\u0443 \u0438 \u043d\u0430\u0447\u0438\u043d\u0430\u0435\u0448\u044c \u0435\u0435 \u043f\u043e\u0432\u043e\u0440\u0430\u0447\u0438\u0432\u0430\u0442\u044c\n";
            w.write(rules);
            w.flush();

            byte byteAr[] = new byte[64 * 1024];

            while (true) {
                String q = "\u041d\u0430 \u043a\u0430\u043a\u043e\u0439 \u0431\u044b \u0443\u0433\u043e\u043b \u043f\u043e\u0432\u0435\u0440\u043d\u0443\u0442\u044c \u0448\u043f\u0438\u043b\u044c\u043a\u0443?\n";
                w.write(q);
                w.flush();

                int c = sin.read(byteAr);
                String request = "";
                for (int i = 0; i < c; i++) {
                    request += (char) byteAr[i];
                }

                if (!request.isEmpty() && request.length() <= 3) {
                    int angel;
                    try {
                        angel = Integer.parseInt(request);
                    } catch (NumberFormatException e) {
                        w.write("\u0427\u0442\u043e-\u0442\u043e \u043f\u043e\u0448\u043b\u043e \u043d\u0435 \u0442\u0430\u043a, \u0438 \u0448\u043f\u0438\u043b\u044c\u043a\u0430 \u043b\u043e\u043c\u0430\u0435\u0442\u0441\u044f\n");
                        w.flush();
                        throw new IOException();
                    }
                    if (angel < 0 || angel >= 360) {
                        w.write("\u0427\u0442\u043e-\u0442\u043e \u043f\u043e\u0448\u043b\u043e \u043d\u0435 \u0442\u0430\u043a, \u0438 \u0448\u043f\u0438\u043b\u044c\u043a\u0430 \u043b\u043e\u043c\u0430\u0435\u0442\u0441\u044f\n");
                        w.flush();
                        throw new IOException();
                    }
                    if (neededAngel < angel) {
                        w.write("\u0422\u044b \u043f\u0440\u0438\u043a\u043b\u0430\u0434\u044b\u0432\u0430\u0435\u0448\u044c \u0443\u0445\u043e \u043a \u0434\u0432\u0435\u0440\u0438 \u0438 \u043f\u043e\u043d\u0438\u043c\u0430\u0435\u0448\u044c, \u0447\u0442\u043e \u043d\u0443\u0436\u043d\u043e \u043f\u043e\u0432\u0435\u0440\u043d\u0443\u0442\u044c \u043d\u0430 \u043c\u0435\u043d\u044c\u0448\u0438\u0439 \u0443\u0433\u043e\u043b\n");
                        w.flush();
                    } else if (neededAngel > angel) {
                        w.write("\u0422\u044b \u043f\u0440\u0438\u043a\u043b\u0430\u0434\u044b\u0432\u0430\u0435\u0448\u044c \u0443\u0445\u043e \u043a \u0434\u0432\u0435\u0440\u0438 \u0438 \u043f\u043e\u043d\u0438\u043c\u0430\u0435\u0448\u044c, \u0447\u0442\u043e \u043d\u0443\u0436\u043d\u043e \u043f\u043e\u0432\u0435\u0440\u043d\u0443\u0442\u044c \u043d\u0430 \u0431\u043e\u043b\u044c\u0448\u0438\u0439 \u0443\u0433\u043e\u043b\n");
                        w.flush();
                    } else {
                        break;
                    }
                } else {
                    w.write("\u0427\u0442\u043e-\u0442\u043e \u043f\u043e\u0448\u043b\u043e \u043d\u0435 \u0442\u0430\u043a, \u0438 \u0448\u043f\u0438\u043b\u044c\u043a\u0430 \u043b\u043e\u043c\u0430\u0435\u0442\u0441\u044f\n");
                    w.flush();
                    throw new IOException();
                }
            }
            long endTime = new Date().getTime();

            if (endTime - startTime < 5000) {
                w.write("\n\u041f\u043e\u043b\u0443\u0447\u0438\u043b\u043e\u0441\u044c! \u0414\u0432\u0435\u0440\u044c \u043e\u0442\u043a\u0440\u044b\u0432\u0430\u0435\u0442\u0441\u044f, \u0438 \u0442\u044b \u0432\u0438\u0434\u0438\u0448\u044c \u043d\u0435\u0434\u043e\u0432\u043e\u043b\u044c\u043d\u043e\u0435 \u043b\u0438\u0446\u043e \u043c\u0438\u0441\u0442\u0435\u0440\u0430 \u0410\u0431\u0435\u0440\u043d\u0430\u0442\u0438,\n" +
                        "\u043a\u043e\u0442\u043e\u0440\u044b\u0439 \u043f\u0440\u043e\u0442\u044f\u0433\u0438\u0432\u0430\u0435\u0442 \u0442\u0435\u0431\u0435 \u043e\u0431\u0440\u044b\u0432\u043e\u043a \u0431\u0443\u043c\u0430\u0433\u0438 \u0441 \u043d\u0430\u0434\u043f\u0438\u0441\u044c\u044e " + FLAG);
            } else {
                w.write("\n\u041f\u043e\u043b\u0443\u0447\u0438\u043b\u043e\u0441\u044c! \u0414\u0432\u0435\u0440\u044c \u043e\u0442\u043a\u0440\u044b\u0432\u0430\u0435\u0442\u0441\u044f, \u0438 \u0442\u044b \u0432\u0438\u0434\u0438\u0448\u044c \u043d\u0435\u0434\u043e\u0432\u043e\u043b\u044c\u043d\u043e\u0435 \u043b\u0438\u0446\u043e \u043c\u0438\u0441\u0442\u0435\u0440\u0430 \u0410\u0431\u0435\u0440\u043d\u0430\u0442\u0438.\n" +
                        " - \u0421\u043b\u0438\u0448\u043a\u043e\u043c \u043c\u0435\u0434\u043b\u0435\u043d\u043d\u043e, \u043c\u043e\u043b\u043e\u0434\u043e\u0439 \u0447\u0435\u043b\u043e\u0432\u0435\u043a, - \u0441\u043b\u044b\u0448\u0438\u0448\u044c \u0442\u044b \u0438 \u043d\u0435\u0434\u043e\u0443\u043c\u0435\u0432\u0430\u0435\u0448\u044c, \u043f\u043e\u0447\u0435\u043c\u0443 \u0442\u0435\u0431\u0435 \u043d\u0435 \u0434\u0430\u043b\u0438 \u043d\u0430\u0433\u0440\u0430\u0434\u0443.");
            }
            w.flush();
            log("disconnected");
        } catch (IOException x) {
            x.printStackTrace();
            log("disconnected");

            try {
                socket.shutdownInput();
                socket.shutdownOutput();
                socket.close();
            } catch (IOException e) {
                log("client error");
            }
        }

    }

    private void log(String s) {
        System.out.println("N" + num + ": " + s);
    }
}