import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class PPC300 extends Thread {
    private static final int PORT = 6663;
    private static final String FLAG = "THIS_IS_STRANGE_DUMP";

    private long num;
    private Socket socket;

    public static void main(String[] ar) {
        ServerSocket ss = null;
        try {
            ss = new ServerSocket(PORT);
            long i = 0;

            System.out.println("Waiting for a client...");

            while (true) {
                new PPC300(i, ss.accept());
                i++;
            }
        } catch (Exception e) {
            System.out.println("Init error: " + e);
        } finally {
            try {
                ss.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public PPC300(long num, Socket s) {
        this.num = num;
        this.socket = s;

        setDaemon(true);
        setPriority(NORM_PRIORITY);
        start();
    }

    @Override
    public void run() {
        try {
            log("connected");
            Date startDate = new Date();
            InputStream sin = socket.getInputStream();
            OutputStream sout = socket.getOutputStream();

            DataOutputStream out = new DataOutputStream(sout);
            Writer w = new OutputStreamWriter(out, "UTF8");

            byte byteAr[] = new byte[64 * 1024];
            String outString;
            String inString;
            Random rand = new Random();
            for (int j = 0; j < 10000; j++) {
                List<String> list = generateHintList(50);
                String key = list.get(new Random().nextInt(list.size()));

                int attemptsCount = 6;
                String dump = "";
                if (j < 10) {
                    dump = generateDump(list);
                } else {
                    int r = rand.nextInt(30);
                    if (r < 2) {
                        dump = generateDumpWithFlag(list);
                    } else {
                        dump = generateDump(list);
                    }
                }

                w.write("\nROBCO INDUSTRIES (TM) TERMINAL PROTOCOL\n" +
                        "ENTER YOUR PASSWORD\n" +
                        "   \n" +
                        "ATTEMPTS LEFT: # # # # # #\n\n" + dump);
                w.flush();

                for (; ; ) {
                    int c = sin.read(byteAr);
                    inString = "";
                    for (int i = 0; i < c; i++) {
                        inString += (char) byteAr[i];
                    }
                    inString = inString.toUpperCase();

                    if (!inString.isEmpty() && list.contains(inString)) {
                        int compareCount = compare(inString, key);
                        if (compareCount == key.length()) {
                            break;
                        } else if (attemptsCount > 0) {
                            outString = attemptString(--attemptsCount);
                            outString += String.format("%d/%d correct\n", compareCount, key.length());
                            w.write(outString);
                            w.flush();
                            if (attemptsCount == 0) {
                                w.write("!!! ATTACK DETECTED !!! NETWORK BLOCKED !!!");
                                w.flush();
                                throw new IOException();
                            }
                        } else {
                            w.write("!!! ATTACK DETECTED !!! NETWORK BLOCKED !!!");
                            w.flush();
                            throw new IOException();
                        }
                    } else {
                        w.write("!!! ATTACK DETECTED !!! NETWORK BLOCKED !!!");
                        w.flush();
                        throw new IOException();
                    }
                    if (new Date().getTime() - startDate.getTime() > 60 * 1000) {
                        w.write("!!! ATTACK DETECTED !!! NETWORK BLOCKED !!!");
                        w.flush();
                        throw new IOException();
                    }
                }
            }
            throw new IOException();

        } catch (IOException x) {
            x.printStackTrace();
            log("disconnected");
            try {
                socket.shutdownInput();
                socket.shutdownOutput();
                socket.close();
            } catch (IOException e) {
                log("client error");
            }
        }
    }

    private static String attemptString(int count) {
        StringBuilder b = new StringBuilder();
        b.append("\nPERMISSION DENIED\n\nATTEMPTS LEFT: ");
        for (int i = 0; i < count; i++) {
            b.append("# ");
        }
        b.append("\n");
        return b.toString();
    }

    private static List<String> generateHintList(int size) {
        List<String> res = new ArrayList<>();
        Random rand = new Random();
        while (res.size() < size) {
            int index = rand.nextInt(ARRAY.length);
            res.add(ARRAY[index].toUpperCase());
        }
        return res;
    }

    private static int compare(String first, String second) {
        if (first.length() != second.length()) {
            return -1;
        }
        if (first.equals(second)) {
            return first.length();
        }
        int count = 0;
        for (int i = 0; i < first.length(); i++) {
            if (first.charAt(i) == second.charAt(i)) {
                count++;
            }
        }
        return count;
    }

    private static String generateDumpWithFlag(List<String> list) {
        list.add(new Random().nextInt(list.size()), "flag:<<" + FLAG + ">>");
        return generateDump(list);
    }

    private static String generateDump(List<String> list) {
        StringBuilder s = new StringBuilder();
        Random rand = new Random();
        for (String str : list) {
            int tempLength = 3 + rand.nextInt(70);
            for (int i = 0; i < tempLength; i++) {
                int tempIndex = rand.nextInt(SPECIAL.length());
                s.append(SPECIAL.charAt(tempIndex));
            }
            s.append(str);
        }
        int leftChars = CHARS_TO_LINE * 2 - s.length() % CHARS_TO_LINE;
        for (int i = 0; i < leftChars; i++) {
            int tempIndex = rand.nextInt(SPECIAL.length());
            s.append(SPECIAL.charAt(tempIndex));
        }
        String tempDump = s.toString();
        int startHex = 10000000 + rand.nextInt(500000);
        int lineCount = tempDump.length() / (2 * CHARS_TO_LINE);
        s = new StringBuilder();
        for (int i = 0; i < lineCount; i++) {
            s.append("0x" + Integer.toHexString(startHex + i * CHARS_TO_LINE));
            s.append(" ");
            s.append(tempDump.substring(i * CHARS_TO_LINE, (i + 1) * CHARS_TO_LINE));
            s.append("   ");

            s.append("0x" + Integer.toHexString(startHex + CHARS_TO_LINE * (i + lineCount)));
            s.append(" ");
            s.append(tempDump.substring((i + lineCount) * CHARS_TO_LINE, (i + 1 + lineCount) * CHARS_TO_LINE));
            s.append("\n");
        }
        return s.toString();
    }

    private void log(String s) {
        System.out.println("N" + num + ": " + s);
    }

    private static final int CHARS_TO_LINE = 20;
    private static final String SPECIAL = "!@#$%^&*()_+-={}|/[]\'\"\\,.<>,.,.0123456789abcdef`~";
    private static final String[] ARRAY = {"astonishment", "baccalaureat", "anticipatory", "carbodiimide", "cosmopolitan", "carbohydrate", "endonuclease", "hydrocephaly", "dehydroarene",
            "fluorocarbon", "inaccessible", "kindergarten", "kiteboarding", "pharmacopeia", "pharmacopeic", "polytheistic", "longboarding", "lonesomeness", "longshoreman", "manufacturer",
            "electroplate", "electroshock", "narcissistic", "neurosurgery", "Northumbrian", "photobiology", "photoemitter", "phototherapy", "plumbylidene", "mobilization", "polytheistic",
            "monofluoride", "radiographer", "positiveness", "reactivation", "sinicization", "presentience", "remonstrance", "recuperation", "reversionary", "successfully", "tachyhydrite",
            "webliography", "warmongering", "suprasternal", "thioaldehyde", "vaticination", "zygomorphous", "zincographic", "postmistress", "quinomethane", "interstellar", "monopolistic",
            "pareiasaurus", "patternmaker", "isolationist", "mythological", "muzzleloader", "microbiology", "microbrowser", "mindboggling", "modification", "oophorectomy", "nebulization",
            "mechanically", "masturbation", "meetinghouse", "oneirologist", "olfactometer", "pluriformity", "pneudraulics", "polyneuritis", "polytheistic", "modification", "moderateness",
            "episcopalian", "everchanging", "exacerbation", "evaporimeter", "deactivation", "decapitation", "denomination", "denivelation", "digitization", "dimerization", "dinucleotide"};
}