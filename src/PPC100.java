import java.io.*;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class PPC100 extends Thread {
    private static final int PORT = 6662;
    private static final String FLAG = "flag{that_the_time_has_come?}";

    private long num;
    private Socket socket;

    public static void main(String[] ar) {
        ServerSocket ss = null;
        try {
            ss = new ServerSocket(PORT);
            long i = 0;

            System.out.println("Waiting for a client...");

            while (true) {
                new PPC100(i, ss.accept());
                i++;
            }
        } catch (Exception e) {
            System.out.println("Init error: " + e);
        } finally {
            try {
                ss.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public PPC100(long num, Socket s) {
        this.num = num;
        this.socket = s;

        setDaemon(true);
        setPriority(NORM_PRIORITY);
        start();
    }

    @Override
    public void run() {
        try {
            log("connected");
            InputStream sin = socket.getInputStream();
            OutputStream sout = socket.getOutputStream();

            Writer w = new OutputStreamWriter(sout);
            byte[] byteAr = new byte[64 * 1024];
            String request;
            w.write("Do you want tell me something?[y/n]");
            w.flush();

            request = "";
            int c = sin.read(byteAr);
            for (int i = 0; i < c; i++) {
                request += (char) byteAr[i];
            }

            if (request.contains("y")) {
                w.write("So what?");
                w.flush();

                request = "";
                c = sin.read(byteAr);
                for (int i = 0; i < c; i++) {
                    request += (char) byteAr[i];
                }

                Date d = new Date();
                String hash = getHashForData(d);
                if (request.equals(hash)) {
                    w.write("Thank you! That's you reward: " + FLAG);
                    w.flush();
                } else {
                    w.write("What? Why now? Time hasn't come yet");
                    w.flush();
                    throw new IOException();
                }
            } else if (request.contains("n")) {
                int afterMinute = 360 + new Random().nextInt(360);
                Date d = new Date();
                Calendar cal = Calendar.getInstance();
                cal.setTime(d);
                cal.add(Calendar.MINUTE, afterMinute);
                String hash = getHashForData(cal.getTime());
                w.write(String.format("Can you tell me %s in %d minute? Thanks", hash, afterMinute));
                w.flush();
            }


            log("disconnected");
        } catch (IOException x) {
            x.printStackTrace();
            log("disconnected");
        } finally {
            try {
                socket.shutdownInput();
                socket.shutdownOutput();
                socket.close();
            } catch (IOException e) {
                log("client error");
            }
        }

    }

    private void log(String s) {
        System.out.println("N" + num + ": " + s);
    }

    private String getHashForData(Date date) {
        String s = new SimpleDateFormat("mm.hh.dd.MM.YY").format(date);
        try {
            return new BigInteger(1, MessageDigest.getInstance("MD5").digest(s.getBytes())).toString(16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
    }
}