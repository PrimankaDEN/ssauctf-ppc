import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class PPC400 extends Thread {
    private static final int PORT = 6664;
    private static final char[] FLAG = "flag{I_HATE_NUMBERS}".toCharArray();

    private long num;
    private Socket socket;

    public static void main(String[] ar) {
        ServerSocket ss = null;
        try {
            ss = new ServerSocket(PORT);
            long i = 0;

            System.out.println("Waiting for a client...");

            while (true) {
                new PPC400(i, ss.accept());
                i++;
            }
        } catch (Exception e) {
            System.out.println("Init error: " + e);
        } finally {
            try {
                ss.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public PPC400(long num, Socket s) {
        this.num = num;
        this.socket = s;

        setDaemon(true);
        setPriority(NORM_PRIORITY);
        start();
    }

    @Override
    public void run() {
        try {
            log("connected");
            InputStream sin = socket.getInputStream();
            OutputStream sout = socket.getOutputStream();

            Writer w = new OutputStreamWriter(sout);
            byte[] byteAr = new byte[64 * 1024];
            String request;
            Random rand = new Random();
            for (char letter : FLAG) {
                int number = 10000 + rand.nextInt(80000);
                String response = getStringForNumber(number);
                w.write(response);
                w.flush();

                request = "";
                int c = sin.read(byteAr);
                for (int i = 0; i < c; i++) {
                    request += (char) byteAr[i];
                }

                if (Integer.parseInt(request) == number) {
                    w.write(letter + "\n");
                    w.flush();
                } else {
                    w.write("Not found. CRITICAL ERROR\nThe system is going down to maintenance mode NOW!");
                    w.flush();
                    throw new IOException();
                }
            }
            w.write("System has recovered from a serious error");
            w.flush();
            log("disconnected");
        } catch (IOException x) {
            x.printStackTrace();
            log("disconnected");
        } finally {
            try {
                socket.shutdownInput();
                socket.shutdownOutput();
                socket.close();
            } catch (IOException e) {
                log("client error");
            }
        }

    }

    private void log(String s) {
        System.out.println("N" + num + ": " + s);
    }

    private static String getStringForNumber(int number) {
        String n = String.valueOf(number);
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < n.length(); i++) {
            char c = n.charAt(i);
            int[] ar = MAP.get(c);
            for (int j = 0; j < ar.length; j++) {
                int val = ar[j] + (j % 2 == 0 ? i * 250 : 0);
                b.append(String.valueOf(val));
                b.append(" ");
            }
        }
        return b.toString();
    }

    private final static Map<Character, int[]> MAP = new HashMap<>();

    static {
        MAP.put('1', new int[]{58, 113, 127, 31, 130, 350});
        MAP.put('2', new int[]{65, 28, 217, 31, 218, 171, 56, 172, 61, 326, 213, 316});
        MAP.put('3', new int[]{69, 32, 216, 31, 218, 160, 66, 164, 217, 177, 207, 333, 55, 332});
        MAP.put('4', new int[]{66, 34, 65, 166, 181, 159, 177, 37, 162, 320});
        MAP.put('5', new int[]{171, 29, 39, 27, 44, 190, 169, 170, 179, 306, 44, 305});
        MAP.put('6', new int[]{172, 27, 40, 27, 48, 301, 179, 302, 174, 161, 48, 156});
        MAP.put('7', new int[]{36, 45, 175, 45, 80, 273});
        MAP.put('8', new int[]{52, 145, 41, 32, 164, 41, 180, 276, 42, 280, 45, 159, 161, 153});
        MAP.put('9', new int[]{173, 175, 59, 173, 56, 44, 174, 41, 182, 325, 47, 320});
        MAP.put('0', new int[]{52, 45, 172, 51, 180, 332, 41, 324, 50, 58});
    }
}